def calc_wiener(edges):

    nodes = set([vertex for edge in edges for vertex in edge])

    W = 0

    for i in nodes:

        distances_for_i = dijkstra_algorithm(edges, i)

        for j in nodes:

            if j > i:

                W += distances_for_i[j]

    return W