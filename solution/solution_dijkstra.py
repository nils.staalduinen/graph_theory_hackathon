import numpy as np
import sys

def dijkstra_algorithm(edges, source):

    nodes = set([vertex for edge in edges for vertex in edge])

    n_atoms = len(set([vertex for edge in edges for vertex in edge]))

    cn_matrix = np.zeros((n_atoms, n_atoms))

    for edge in edges:

        i = edge[0]

        j = edge[1]

        cn_matrix[i,j] += 1

        cn_matrix[j,i] += 1

    
    #INIT
 
    distances = {v: sys.maxsize for v in range(n_atoms)}

    distances[source] = 0

    queue = list(nodes)

    #Loop

    while queue:

        filtered_distances = {v: d for v, d in distances.items() if v in queue}

        v = min(filtered_distances, key=filtered_distances.get)

        queue.remove(v)

        adjacent_vertices = np.argwhere(cn_matrix[v] == 1).flatten()

        for u in adjacent_vertices:

            dist_uv = distances[v] + 1

            if dist_uv < distances[u]:

                distances[u] = dist_uv

    return distances